# Extract mail to file that matches supplied pattern

First parameter is a Python regex to use in a .search() operation

python ~/gitlocal/mailextract/varmailextract.py 'freshclam'


## /tmp/mail.txt - primary output file - contains 'matching' blocks

This is the file containing [target] blocks of mail,
that matched the given string/regex


## /tmp/nonmatching.txt - what is this file?

The non-matches (things you want to remain) in mail
are written to this file so that after the process
is completed you can do something like....

cp /tmp/nonmatching.txt /var/mail/someuser
chown someuser:mail /var/mail/someuser

or perhaps just

cat /tmp/nonmatching.txt > /var/mail/someuser


## What might this script help with?

Rather than clear out all mails in emergency such as shown below:

> /var/mail/someuser

echo 'd *' | mail -N


## String format / regex

Any valid string or Python regex can be used

If you are not sure of the syntax for python regex then
just websearch an online tool and perfect your syntax there first


## Overriding namedtuple - halfway to OO

This script would primarily be used by Systems Administrators / Site Reliability Engineers
and so we have avoided any heavy use of Object Orientation.

Overriding a namedtuple might be considered halfway to OO
as it provides a little bit of encapsulation but without having
getters, setters, and the full OO setup.

The tradeoff is that wrapping _replace() rather than using a true
fully implemented object, requires that results are re-assigned over
the original as _replace() is going to create a copy rather than
affecting the original object (extension of namedtuple)
