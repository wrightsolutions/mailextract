# Simple script progressing to include fully Object Oriented mail items/targets

This project will include elements of:  

* Automated Testing
* Object Orientation - classes and methods


## Order in which this project progressed [ the iterations ]
* Simple script
* Utilising NamedTuples - a sort of halfway to OO
* Test harnesses
* Timing benchmark against 641MB input
* Split of older version (now labelled varmailextract_rent)
* Newer version varmailextract3.py to include a refactor so mail items/targets
are full objects
* Timing benchmark against 641MB input for newer refactored code


## Overriding namedtuple - halfway to OO

This script would primarily be used by Systems Administrators / Site Reliability Engineers
and so we have avoided any heavy use of Object Orientation.

Overriding a namedtuple might be considered halfway to OO
as it provides a little bit of encapsulation but without having
getters, setters, and the full OO setup.

The tradeoff is that wrapping _replace() rather than using a true
fully implemented object, requires that results are re-assigned over
the original as _replace() is going to create a copy rather than
affecting the original object (extension of namedtuple)
